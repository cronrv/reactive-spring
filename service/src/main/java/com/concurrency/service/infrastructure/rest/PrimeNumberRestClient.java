package com.concurrency.service.infrastructure.rest;

import com.concurrency.service.api.exceptions.ServiceException;
import com.concurrency.service.entity.GatewayToken;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@AllArgsConstructor
public class PrimeNumberRestClient implements NumberRestClient<String>{

    private final SecurityTokenRestClient<GatewayToken> gatewayTokenSecure;

    private final String clientIdHeader;
    private final String clientId;
    private final String gatewayHost;
    private final String primeMethod;
    private final String tokenParam;

    public String getPrimeFromOutside() {
        GatewayToken token = gatewayTokenSecure.getCachedToken();

        ResponseEntity<String> primeNumberResponse = new RestTemplate().exchange(RequestEntity
                .get(getUri(token))
                .header(clientIdHeader, clientId)
                .build(), String.class);

        if(!HttpStatus.OK.equals(primeNumberResponse.getStatusCode())
                || !primeNumberResponse.hasBody()){
            throw new ServiceException("Bad gateway response received");
        }

        return primeNumberResponse.getBody();
    }

    private String getUri(GatewayToken token) {
        return new StringBuffer(gatewayHost)
                .append(primeMethod)
                .append(tokenParam)
                .append(token.getToken())
                .toString();
    }
}
