package com.concurrency.service.infrastructure.rest;

import com.concurrency.service.api.exceptions.ServiceException;
import com.concurrency.service.entity.GatewayToken;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;

@RequiredArgsConstructor
@Slf4j
public class GatewayTokenRestClient implements SecurityTokenRestClient<GatewayToken> {

    private GatewayToken gatewayToken;

    @NonNull
    private final String clientIdHeader;
    @NonNull
    private final String clientId;
    @NonNull
    private final String gatewayHost;
    @NonNull
    private final String tokenMethod;

    public synchronized GatewayToken getCachedToken() {
        if(gatewayToken == null || !validateToken(gatewayToken)){
            gatewayToken = getToken();
        }
        return new ModelMapper().map(gatewayToken, GatewayToken.class);
    }

    public synchronized boolean validateToken(GatewayToken gatewayToken) {
        return Instant.now()
                .isBefore(gatewayToken.getIssuedTime().plusMillis(gatewayToken.getExpirationTime().toEpochMilli()) );
    }

    public GatewayToken getToken() {
        log.info("Requesting a new token");
        ResponseEntity<GatewayToken> gatewayResponse = new RestTemplate().exchange(RequestEntity
                .get(gatewayHost + tokenMethod)
                .header(clientIdHeader, clientId)
                .build(), GatewayToken.class);

        if(!HttpStatus.OK.equals(gatewayResponse.getStatusCode())
                || !gatewayResponse.hasBody()){
            throw new ServiceException("Bad gateway response received");
        }

        return gatewayResponse.getBody();
    }

}
