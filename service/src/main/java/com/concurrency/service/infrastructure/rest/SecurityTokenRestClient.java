package com.concurrency.service.infrastructure.rest;

public interface SecurityTokenRestClient<T> {
    T getCachedToken();
    boolean validateToken(T token);
    T getToken();
}
