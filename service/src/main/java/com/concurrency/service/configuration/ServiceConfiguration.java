package com.concurrency.service.configuration;

import com.concurrency.service.entity.GatewayToken;
import com.concurrency.service.infrastructure.rest.GatewayTokenRestClient;
import com.concurrency.service.infrastructure.rest.NumberRestClient;
import com.concurrency.service.infrastructure.rest.PrimeNumberRestClient;
import com.concurrency.service.infrastructure.rest.SecurityTokenRestClient;
import com.concurrency.service.repository.NumberRepository;
import com.concurrency.service.repository.PrimeNumberRepository;
import com.concurrency.service.usecase.NumberUsecase;
import com.concurrency.service.usecase.PrimeNumber;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class ServiceConfiguration {

    @Value("${gateway.rest-client.client-id.header}")
    private String clientIdHeader;
    @Value("${gateway.rest-client.client-id.value}")
    private String clientId;
    @Value("${gateway.rest-client.host}")
    private String gatewayHost;
    @Value("${gateway.rest-client.token-method}")
    private String tokenMethod;
    @Value("${gateway.prime-number-method}")
    private String primeMethod;
    @Value("${gateway.prime-number-token-param}")
    private String tokenParam;

    @Bean("primeNumber")
    public NumberUsecase<List<String>> getPrimeNumber(
            @Qualifier("primeNumberRepository") NumberRepository<List<String>> numberRepository) {
        return new PrimeNumber(numberRepository);
    }

    @Bean("primeNumberRepository")
    public NumberRepository<List<String>> getPrimeNumberRepository(
            @Qualifier("primeNumberRestClient") NumberRestClient<String> numberRestClient) {
        return new PrimeNumberRepository(numberRestClient);
    }

    @Bean("primeNumberRestClient")
    public NumberRestClient<String> getPrimeNumberRepository(
            @Qualifier("gatewayTokenRestClient") SecurityTokenRestClient<GatewayToken> securityTokenRestClient) {
        return new PrimeNumberRestClient(securityTokenRestClient,
                clientIdHeader, clientId, gatewayHost, primeMethod, tokenParam);
    }

    @Bean("gatewayTokenRestClient")
    public SecurityTokenRestClient<GatewayToken> getGatewayTokenRestClient() {
        return new GatewayTokenRestClient(clientIdHeader, clientId, gatewayHost, tokenMethod);
    }
}
