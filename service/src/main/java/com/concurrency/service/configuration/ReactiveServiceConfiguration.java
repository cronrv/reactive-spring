package com.concurrency.service.configuration;

import com.concurrency.service.api.rest.NumberRestEntryPoint;
import com.concurrency.service.api.rest.ReactivePrimeNumberHandler;
import com.concurrency.service.infrastructure.rest.NumberRestClient;
import com.concurrency.service.repository.NumberRepository;
import com.concurrency.service.repository.ReactivePrimeNumberRepository;
import com.concurrency.service.usecase.NumberUsecase;
import com.concurrency.service.usecase.ReactivePrimeNumber;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

@Profile("reactive")
@Configuration(proxyBeanMethods = false)
@EnableAutoConfiguration(exclude = {WebMvcAutoConfiguration.class})
@EnableWebFlux
public class ReactiveServiceConfiguration {

    @Bean
    public RouterFunction<ServerResponse> route(
            @Qualifier("reactivePrimeNumberHandler") NumberRestEntryPoint<Mono<ServerResponse>, ServerRequest>
                    reactivePrimeNumberHandler) {
        return RouterFunctions.route(GET("/api/v2/prime/numbers").and(accept(MediaType.APPLICATION_JSON)),
                reactivePrimeNumberHandler::getNumbers);
    }

    @Bean("reactivePrimeNumberHandler")
    public NumberRestEntryPoint<Mono<ServerResponse>, ServerRequest> getReactivePrimeNumberHandler(
            @Qualifier("reactivePrimeNumber") NumberUsecase<Flux<String>> numberRepository) {
        return new ReactivePrimeNumberHandler(numberRepository);
    }

    @Bean("reactivePrimeNumber")
    public NumberUsecase<Flux<String>> getPrimeNumber(
            @Qualifier("reactivePrimeNumberRepository") NumberRepository<Flux<String>> numberRepository) {
        return new ReactivePrimeNumber(numberRepository);
    }

    @Bean("reactivePrimeNumberRepository")
    public NumberRepository<Flux<String>> getPrimeNumberRepository(
            @Qualifier("primeNumberRestClient") NumberRestClient<String> numberRestClient) {
        return new ReactivePrimeNumberRepository(numberRestClient);
    }
}
