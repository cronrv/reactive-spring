package com.concurrency.service.usecase;

import com.concurrency.service.repository.NumberRepository;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class PrimeNumber implements NumberUsecase<List<String>>{

    private final NumberRepository<List<String>> numberRepository;

    @Override
    public List<String> getNumbers(int range) {
        return numberRepository.getNumbers(range);
    }
}
