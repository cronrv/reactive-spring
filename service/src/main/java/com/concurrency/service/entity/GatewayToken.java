package com.concurrency.service.entity;

import lombok.Data;

import java.time.Instant;

@Data
public class GatewayToken {
    private String token;
    private Instant issuedTime;
    private Instant expirationTime;
}
