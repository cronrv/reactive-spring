package com.concurrency.service.repository;

import com.concurrency.service.infrastructure.rest.NumberRestClient;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

@AllArgsConstructor
public class ReactivePrimeNumberRepository implements NumberRepository<Flux<String>> {

    private final NumberRestClient<String> numberRestClient;

    public Flux<String> getNumbers(int range) {
        return Flux.range(0, range)
                .publishOn(Schedulers.parallel())
                .map(list -> numberRestClient.getPrimeFromOutside());
    }
}
