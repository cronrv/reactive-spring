package com.concurrency.service.repository;

import com.concurrency.service.infrastructure.rest.NumberRestClient;
import com.concurrency.service.usecase.NumberUsecase;
import com.concurrency.service.usecase.PrimeNumber;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@AllArgsConstructor
public class PrimeNumberRepository implements NumberRepository<List<String>> {

    private final NumberRestClient<String> numberRestClient;

    public List<String> getNumbers(int range) {
        return IntStream.range(0, range)
                .parallel()
                .mapToObj(list -> numberRestClient.getPrimeFromOutside())
                .collect(Collectors.toList());
    }

    @Bean("reactivePrimeNumber")
    public NumberUsecase<List<String>> getPrimeNumber(
            @Qualifier("reactivePrimeNumberRepository") NumberRepository<List<String>> numberRepository) {
        return new PrimeNumber(numberRepository);
    }

    @Bean("reactivePrimeNumberRepository")
    public NumberRepository<List<String>> getPrimeNumberRepository(
            @Qualifier("reactivePrimeNumberRestClient") NumberRestClient<String> numberRestClient) {
        return new PrimeNumberRepository(numberRestClient);
    }
}
