package com.concurrency.service.api.rest;

import com.concurrency.service.api.exceptions.ServiceException;
import com.concurrency.service.usecase.NumberUsecase;
import lombok.AllArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@AllArgsConstructor
public class ReactivePrimeNumberHandler implements NumberRestEntryPoint<Mono<ServerResponse>, ServerRequest> {

    private NumberUsecase<Flux<String>> primeNumber;

    @Override
    public Mono<ServerResponse> getNumbers(ServerRequest request) {
        return ServerResponse.ok().body(BodyInserters.fromPublisher(
                primeNumber.getNumbers(Integer.parseInt(
                        request.queryParam("range").orElseThrow(() -> new ServiceException("Missing param")))),
                ParameterizedTypeReference.forType(String.class)
        ));
    }
}
