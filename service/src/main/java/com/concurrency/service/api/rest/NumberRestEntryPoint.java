package com.concurrency.service.api.rest;

public interface NumberRestEntryPoint<T, U> {
    T getNumbers(U requestData);
}
