package com.concurrency.service.api.rest;

import com.concurrency.service.usecase.NumberUsecase;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class PrimeNumberController implements NumberRestEntryPoint<List<String>, String> {

    private final NumberUsecase<List<String>> primeNumber;

    @Override
    @GetMapping("/prime/numbers")
    public List<String> getNumbers(@RequestParam String range) {
        return primeNumber.getNumbers(Integer.parseInt(range));
    }
}
