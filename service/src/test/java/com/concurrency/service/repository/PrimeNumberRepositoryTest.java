package com.concurrency.service.repository;

import com.concurrency.service.entity.PrimeNumberSet;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SpringBootTest
@Slf4j
class PrimeNumberRepositoryTest {

    @Autowired
    private PrimeNumberRepository primeNumberRepository;

    @Test
    void getPrimeNumbers() {
        List<PrimeNumberSet> data = IntStream.range(0, 10)
                .parallel()
                .mapToObj(n -> getData(n, new Random().nextInt(5)))
                .filter(Objects::nonNull)
                .peek(n -> log.info("Finishing: " + n.getOrder()))
                .collect(Collectors.toList());
        data.forEach(this::logData);
        data.stream().map(PrimeNumberSet::getPrimeNumbers)
                .map(List::isEmpty)
                .forEach(Assertions::assertFalse);
    }

    private PrimeNumberSet getData(int n, int listNumber) {
        return PrimeNumberSet.builder()
                .primeNumbers(primeNumberRepository.getNumbers(listNumber))
                .listNumber(listNumber)
                .order(n)
                .build();
    }

    private void logData(PrimeNumberSet n) {
        System.out.printf("\nResponse from service, id: %s\n data: %s",
                n.getOrder(),
                n);
    }
}