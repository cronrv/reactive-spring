package com.concurrency.service.infrastructure.rest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class GatewayTokenSecureTest {

    @Autowired
    private GatewayTokenRestClient gatewayTokenSecure;

    @Test
    void getGatewayToken() {
        assertNotNull(gatewayTokenSecure.getToken());
    }
}