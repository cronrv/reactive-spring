package com.concurrency.gateway.configuration;

import com.concurrency.gateway.entity.Credentials;
import com.concurrency.gateway.entity.GatewayToken;
import com.concurrency.gateway.usecase.Service;
import com.concurrency.gateway.usecase.ServiceChain;
import com.concurrency.gateway.usecase.gateway.GatewayService;
import com.concurrency.gateway.usecase.number.PrimeNumberService;
import com.concurrency.gateway.usecase.validator.ClientIdValidator;
import com.concurrency.gateway.usecase.validator.DefaultValidator;
import com.concurrency.gateway.usecase.validator.TokenValidator;
import com.concurrency.gateway.usecase.validator.ValidatorHook;
import com.concurrency.gateway.util.ValidatorType;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfiguration {

    @Value("${gateway.rest-client.client-id.value}")
    private String clientId;
    @Value("${gateway.rest-client.delay}")
    private int waitInMS;
    @Value("${gateway.rest-client.expiration-hours}")
    private int amountToAdd;
    @Value("${gateway.rest-client.prime-number-list-limit}")
    private int limit;

    @Bean("gatewayService")
    public Service<GatewayToken, String> getGatewayService(
            @Qualifier("validatorHook") ServiceChain<Boolean, String, ValidatorType> validatorHook){
        return new GatewayService(validatorHook, waitInMS, amountToAdd);
    }

    @Bean("primeNumberService")
    public Service<String, Credentials> getPrimeNumberService(
            @Qualifier("validatorHook") ServiceChain<Boolean, String, ValidatorType> validatorHook){
        return new PrimeNumberService(validatorHook, limit, waitInMS);
    }

    @Bean("validatorHook")
    public ServiceChain<Boolean, String, ValidatorType> getServiceChainValidatorHook(
            @Qualifier("clientIdValidator") ServiceChain<Boolean, String, ValidatorType> clientIdValidator){
        return new ValidatorHook(clientIdValidator);
    }

    @Bean("clientIdValidator")
    public ServiceChain<Boolean, String, ValidatorType> getClientIdValidator(
            @Qualifier("tokenValidator") ServiceChain<Boolean, String, ValidatorType> tokenValidator){
        return new ClientIdValidator(tokenValidator, clientId);
    }

    @Bean("tokenValidator")
    public ServiceChain<Boolean, String, ValidatorType> getTokenValidator(
            @Qualifier("defaultValidator") ServiceChain<Boolean, String, ValidatorType> defaultValidator){
        return new TokenValidator(defaultValidator);
    }

    @Bean("defaultValidator")
    public ServiceChain<Boolean, String, ValidatorType> getDefaultValidator(){
        return new DefaultValidator();
    }
}
