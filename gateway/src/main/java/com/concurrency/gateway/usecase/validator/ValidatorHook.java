package com.concurrency.gateway.usecase.validator;

import com.concurrency.gateway.usecase.ServiceChain;
import com.concurrency.gateway.util.ValidatorType;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ValidatorHook implements ServiceChain<Boolean, String, ValidatorType> {

    private final ServiceChain<Boolean, String, ValidatorType> clientIdValidator;

    @Override
    public Boolean processData(String requestData) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Boolean checkChainElement(String requestData, ValidatorType type) {
        return clientIdValidator.checkChainElement(requestData, type);
    }
}
