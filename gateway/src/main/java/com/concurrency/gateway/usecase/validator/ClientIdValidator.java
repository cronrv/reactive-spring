package com.concurrency.gateway.usecase.validator;

import com.concurrency.gateway.usecase.ServiceChain;
import com.concurrency.gateway.util.ValidatorType;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ClientIdValidator implements ServiceChain<Boolean, String, ValidatorType> {

    private final ServiceChain<Boolean, String, ValidatorType> tokenValidator;

    private final String clientId;

    @Override
    public Boolean processData(String requestData) {
        return requestData.equals(this.clientId);
    }

    @Override
    public Boolean checkChainElement(String requestData, ValidatorType type) {
        return type.equals(ValidatorType.CLIENT_ID)
                ? processData(requestData)
                : tokenValidator.checkChainElement(requestData, type);
    }
}
