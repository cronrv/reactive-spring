package com.concurrency.gateway.usecase.gateway;

import com.concurrency.gateway.api.exceptions.ServiceException;
import com.concurrency.gateway.entity.GatewayToken;
import com.concurrency.gateway.usecase.Service;
import com.concurrency.gateway.usecase.ServiceChain;
import com.concurrency.gateway.util.ValidatorType;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

@Slf4j
@AllArgsConstructor
public class GatewayService implements Service<GatewayToken, String> {

    private final ServiceChain<Boolean, String, ValidatorType> validatorHook;

    private final int waitInMS;
    private final int amountToAdd;

    public GatewayToken processData(String clientId) {
        log.info("Initiating validation");
        if (Boolean.FALSE.equals(validatorHook.checkChainElement(clientId, ValidatorType.CLIENT_ID))){
            throw new ServiceException("Bad client id");
        }

        log.info("Initiating wait");
        try {
            Thread.sleep(waitInMS);
        } catch (InterruptedException e) {
           log.error(e.getMessage(), e);
           Thread.currentThread().interrupt();
        }

        log.info("Returning token");
        Instant now = Instant.now();
        return GatewayToken.builder()
                .token(UUID.randomUUID().toString())
                .issuedTime(now)
                .expirationTime(now.plus(amountToAdd, ChronoUnit.HOURS)).build();
    }
}
