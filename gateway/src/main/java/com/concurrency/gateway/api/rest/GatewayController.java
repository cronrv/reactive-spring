package com.concurrency.gateway.api.rest;

import com.concurrency.gateway.entity.Credentials;
import com.concurrency.gateway.entity.GatewayToken;
import com.concurrency.gateway.usecase.Service;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/gateway")
@AllArgsConstructor
@Slf4j
public class GatewayController {

    private final Service<GatewayToken, String> gatewayService;
    private final Service<String, Credentials> primeNumberService;

    @GetMapping(value = "/token")
    public ResponseEntity<GatewayToken> getToken(
            @RequestHeader(value = "${gateway.rest-client.client-id.header}") String clientId) {
        return ResponseEntity.ok(gatewayService.processData(clientId));
    }

    @GetMapping("/primeNumbers")
    public ResponseEntity<String> getPrimeNumbers(
            @RequestParam String token,
            @RequestHeader(value = "${gateway.rest-client.client-id.header}") String clientId) {
        return ResponseEntity.ok(
                primeNumberService.processData(
                        Credentials.builder()
                                .token(token)
                                .clientId(clientId)
                                .build()));
    }
}
