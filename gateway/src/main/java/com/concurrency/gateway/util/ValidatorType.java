package com.concurrency.gateway.util;

public enum ValidatorType {
    CLIENT_ID,
    TOKEN;
}
