package com.concurrency.gateway.entity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.Instant;

@Data
@Builder
public class GatewayToken {
    private String token;
    private Instant issuedTime;
    private Instant expirationTime;
}
